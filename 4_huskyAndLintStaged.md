# Configuração Husky e lint-staged

## Instalar dependências

```
yarn add -D lint-staged husky
```



## Criar ficheiro .lintstagedrc.json
criar na raiz do projeto o ficheiro .lintstagedrc.json
```
./Configuracoes/.lintstagedrc.json
```

## Criar ficheiro .huskyrc.json
criar na raiz do projeto o ficheiro .huskyrc.json
```
./Configuracoes/.huskyrc.json
```