# CONFIGURAÇÔES GIT

## Inicializar Git
Abrir pasta que queremos criar o repositório
Na linha de comandos executar:
```
	git init
```

## Abrir ficheiro de configurações do git
```
git config --local --edit
```


 - --local : Apenas do repositório
 - --global : Mudar em qualquer repositório com base no user,
 - --system : Para qualquer utilizador

### Adicionar
```
[user]
	name = Micael Costa
	email = josemicael16@hotmail.com
[credential]
	provider = generic
[core]
	editor = code --wait
[push]
	followTags = true
[alias]
	c = !git add --all && git commit -m
	s = !git status -s
	l = !git log --pretty=format:'%C(blue)%h %C(red)%d %C(white)%s - %C(cyan)%cn, %C(green)%cr'
	amend = !git add --all && git commit --amend --no-edit
	count = !git shortlog -s --grep
```

editor - É para abrir o ficheiro de configuração no visual studio code

followTags - é para enviar as tags anotadas para o servidor (github, gitlab)

## Tipos de comandos
- git s - mostra o status dos ficheiros
- git c - adicionar todos os ficheiros e faz commit
- git l - mostra os logs com um formato e cores com melhor visualização
- git amend - junta um commit no commit anterior
- git count - contabilizar o numero de commits com base num tipo
- git tag : ver a lista de tags
- git tag "1.0" : cria uma tag normal
- git tag - a "1.0" :  : cria uma tag anotada. Associada a uma release

## Git Count tipos
https://www.conventionalcommits.org/en/v1.0.0/
- chore
- test
- feat
- fix
- refactor

## Adicionar dependicia

```
yarn add -D git-commit-msg-linter
```
Esta biblioteca é para dar erro caso não se sigam as convenções nos commits presentes no ponto anterior


## Adicionar .gitignore

```
	node_modules
	.vscode
	yarn.lok
	package-lock.json
	yarn-error.log
	coverage	
```

## fazer commit das alterações
Para testar que a dependência do linter está a funcionar, executar o seguinte comando e verificar se deu erro
```
git c "Add commit linter"
``` 

Caso tenha dado erro, está tudo a funcionar e basta executar o seguinte comando

```
git c "chore: Add commit linter"
``` 