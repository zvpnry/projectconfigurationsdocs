# Configuração ESLint

## Instalar dependências

```
yarn add -D eslint eslint-config-standard-with-typescript eslint-plugin-import eslint-plugin-promise @typescript-eslint/eslint-plugin
eslint-plugin-node
```


eslint-config-standard-withtypescript@11 por causa de conflitos 

eslint-plugin-standard - caso seja preciso


## Criar ficheiro .eslintrc
criar na raiz do projeto o ficheiro .eslintrc
```
./Configuracoes/.eslintrc
```

## Criar ficheiro .eslintignore
criar na raiz do projeto o ficheiro .eslintignore
```
./Configuracoes/.eslintignore
```