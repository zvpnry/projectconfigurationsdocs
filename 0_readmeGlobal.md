# Configurações Projeto


## Inicializar npm
```
npm init
```
ou 
```
npm init -y
```

## Configurar Git
```
./1_readmeGIT.md
```

## Configurar Typescript
```
./2_readmeTypescript.md
```

## Configurar linter
```
./3_readmeLinter.md
```

## Configurar husky e lint-staged
```
./4_huskyAndLintStaged.md
```

## Configurar jest
```
./4_huskyAndLintStaged.md
```